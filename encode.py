from PIL import Image
import ntpath

def encode(sourcefile, destinationfile="img.png", imagewidth=1280):
    """Encode a file to a png picture

    Keyword arguments:
    sourcefile -- the source file (required)
    destinationfile -- the destination file (default "img.png")
    imagewidth -- the width of the image to be generated in pixel (default 1280 pixel)
    """
    try:
        #open file in read binary mode
        filetoencode = open(sourcefile, "rb")

        #initialize the bytes variable
        databytes = b""

        databytes = bytes(ntpath.basename(filetoencode.name) + "\n", "UTF-8")
        databytes += filetoencode.read()
    finally:
        filetoencode.close()

    #if there are less bytes than the imagewidth, set image width to that value
    if len(databytes) < imagewidth * 3:
        width = int(len(databytes)/3+1)
    else:
        width = imagewidth
    #height
    height = int(len(databytes)/(imagewidth * 3)) + 1

    #fill the rest of the image with zeros
    if len(databytes) < 3 * width * height:
        for x in range(3 * width * height - len(databytes)):
            databytes += b"\0"

    #new image with 1 bit depth
    img = Image.new('RGB', (width, height))

    #print(databytes)
    #create the image from the read bytes
    img.frombytes(databytes)
    try:
        #and save it as an image
        img.save(destinationfile)
    finally:
        img.close()
