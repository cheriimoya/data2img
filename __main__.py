import sys
from decode import decode
from encode import encode

def usage():
    print("Usage: python encode|decode sourcefile [destination file]")

def main():
    if len(sys.argv) < 3 or len(sys.argv) > 4:
        usage()
        return

    source = sys.argv[2]
    dest = ""

    if len(sys.argv) == 4:
        dest = sys.argv[3]

    if sys.argv[1] == "encode":
        print("encoding...")
        if dest:
            encode(source, dest)
        else:
            encode(source)
        return

    if sys.argv[1] == "decode":
        print("decoding...")
        if dest:
            decode(source, dest)
        else:
            decode(source)
        return

    usage()

if __name__ == "__main__":
    main()
