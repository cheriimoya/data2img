from PIL import Image

def decode(sourcefile="img.png", destinationfile=""):
    """Decode a previously encoded png picture back to a file

    Keyword arguments:
    sourcefile -- the source file (default "img.png")
    destinationfile -- the destination file (default "")
    """

    try:
        img = Image.open(sourcefile)
        byte = img.tobytes()
        name = b""
        for x in range(len(byte)):
            #10 is equal to \n
            if byte[x] == 10:
                break
        name = byte[:x]
        dest = open(name, "wb")
        #dest = open(destinationfile, "wb")
        dest.write(byte[x + 1:])
    finally:
        img.close()
        dest.close()
